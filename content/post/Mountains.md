+++
image="https://gitlab.com/seanrobson/hugo/raw/19adf3eb4a476fe44b34d378b7579ea81e3347bb/static/img/b_bike.jpg"
#image = "/img/b_bike.jpg"
categories = []
date = "2017-02-13"
menu = ""
tags = []
title = "Mountains of Misery (MoM)"
+++

Previous Results
==========

This is a new blog connect my interests in sports with data. First off, I'd like to share an [interactive app](http://www.seanrobson.com:3838/apps/mountains/) that I built last year to compile the results for all Mountains of Misery participants. For those not familiar, [Mountains of Misery](http://www.mountainsofmisery.com) is a ~104 mile cycling event for the most popular event. For more experienced riders, there is a longer double-metric ride (~125 miles). It's a beatiful ride through the mountains just west of Blacksburg, Virginia. I have been both a rider and a volunteer. This is a challenging ride especially the last 4 miles up Mountain Lake, which is climb averaging between 12-16% grade. The event is made even better by a great group of volunteers, many of whom are part of the awesome [TriAdventure](http://www.triadventure.com) community.

I welcome comments and suggestions.
